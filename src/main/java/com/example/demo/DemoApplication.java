package com.example.demo;

import com.sun.javafx.beans.IDProperty;
import lombok.*;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.reactivestreams.Publisher;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.lang.annotation.Documented;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

@SpringBootApplication
//@RestController
public class DemoApplication {


	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		System.out.println("test");

	}
}
	@Component
	@RequiredArgsConstructor
	@Log4j2
	class SampleDateInitializer{
		private  final ReservationRepository reservationRepository;

		@EventListener(ApplicationReadyEvent.class)
		public void initialize(){

			//Flux<String> names = Flux.just("Josh", "Corn", "Stefan", "Olga", "Sara");
			//Flux<Reservation> reservationFlux = names.map(name -> new Reservation(null, name));
			//Publisher<Publisher<Reservation>> reservationFlux1 = reservationFlux.map(re -> reservationRepository.save(re));
//now with flat map
			//Flux<Reservation> reservationFlux1 = reservationFlux.flatMap(re -> reservationRepository.save(re));
			//Flux<Reservation> saved = reservationFlux.flatMap((this.reservationRepository::save));
			var saved = Flux //java 11
			//Flux<Reservation> saved = Flux
					.just("Josh", "Corn", "Stefan", "Olga", "Sara")
					.map(name -> new Reservation(null, name))
					.flatMap(this.reservationRepository::save);




			//to triger to start
			//saved.subscribe();

			reservationRepository
					.deleteAll() //should guarantee before anything else
					.thenMany(saved)
					.thenMany(this.reservationRepository.findAll())
					/* //1
					.subscribe(new Consumer<Reservation>() {
						@Override
						public void accept(Reservation reservation) {

							public void accept(Reservation reservation){
								log.info(reservation);
							}
						}
					})*/
					/*//2
					.subscribe(( reservation -> log.info(reservation);
					);*/
					//below code is possible not to use delete all but nor recommended
					//only with interactions blocking code
					//1 event loop per core
					//.subscribeOn(Schedulers.fromExecutor(Executors.newSingleThreadExecutor()))
					.subscribe(log::info);





		}
	}




	interface ReservationRepository extends ReactiveCrudRepository<Reservation, String>{

	}


	@Document
	@Data
	@AllArgsConstructor
	@NoArgsConstructor
	class Reservation{
		@Id
		private String id;
		private String name;
	}


